# Mobify Progressive Mobile Scaffold

## Requirements

- [Git](https://git-scm.com/)
- We recommend you use [nvm](https://github.com/creationix/nvm#installation) to
manage node and npm versions.
- Node ^8.x [LTS](https://github.com/nodejs/LTS#lts-schedule)
- npm ^5.x

## Directory

This repository contains scaffold projects for Mobify's platform.

### [Web](/web)
### [App](/native)
### [AMP](/amp)

## Folder Structure

    .
    ├── web                             # PWA
        ├── app                         # Main project source folder
            ├── components              # Reusable React component (NOT connected to redux)
                ├── ...
                    ├── index.jsx       # Main component file
                    ├── _base.scss      # Component specific styles
                    ├── test.js         # Component specific tests
            ├── containers              # React containers (connected to redux)
                ├── ...
                    ├── partials        # Blocks of component in a container
                    ├── container.jsx   # Main container file
                    ├── _base.scss      # Container specific styles
                    ├── actions.js      # Container specific Redux actions
                    ├── constants.js    # Container specific constants
                    ├── reducer.js      # Container specific Redux reducer
                    ├── selector.js     # Container specific selector
                ├── reducer             # Main UI reducer
                ├── templates           # Lazy loading higher order containers
            ├── modals                  # React modals (connected to redux)
                ├── ...                 
                ├── index.jsx           # Modal Manager
            ├── config                  # Configurations
            ├── connectors              # Integration Manager connectors (network related code)
            ├── connector-extension     # Integration Manager connector customizations
            ├── preloader               # Fast initial page painting
            ├── store                   # Redux store
            ├── static                  # Application static files such as svg, img, fonts
            ├── style                   # Sass files
            ├── utils                   # Utility functions
            ├── vendor                  # Third party scripts
        ├── dev-server                  # The webpack development server and certificate files
        ├── messaging                   # Web push notification
        ├── non-pwa                     # Web push notification
        ├── reports                     # Contains debugging reports such as dependency graph
        ├── scripts                     # The npm scripts for build, test, deploy tasks
        ├── tests                       # Automation tests
            ├── e2e
            ├── performance/lighthouse
        ├── webpack                     # Webpack configurations for production and development
        ├── worker                      # Service worker related code
        ├── ...
        ├── .eslintrc.yml               # Eslint configuration
        ├── .babelrc                    # ES6+ Compilation configuration
        ├── .sass-lint.yml              # SASS lint configuration
        ├── service-worker-loader.js    # The script that import the service worker
        ├── package.json
        ├── README.md
    ├── amp
    ├── native